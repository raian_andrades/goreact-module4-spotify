import React from "react";

import { Container, Search, User } from "./styles";

const Header = () => (
    <Container>
        <Search>
            <input placeholder="Search" />
        </Search>

        <User>
            <img
                src="https://avatars0.githubusercontent.com/u/6072922?v=4"
                alt="User avatar"
            />
            Raian de Andrades
        </User>
    </Container>
);

export default Header;
